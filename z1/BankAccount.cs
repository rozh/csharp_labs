﻿using System;

namespace z7
{
    /// <summary>
    /// Типы счетов
    /// </summary>
    enum BankAccountType
    {
        /// <summary>
        /// Текущий
        /// </summary>
        Current,

        /// <summary>
        /// Расчетный
        /// </summary>
        Payment,

        /// <summary>
        /// Бюджетный
        /// </summary>
        Budget,

        /// <summary>
        /// Корреспондентский
        /// </summary>
        Сorrespondent
    }

    /// <summary>
    /// Счет
    /// </summary>
    class BankAccount
    {
        /// <summary>
        /// Баланс
        /// </summary>
        private decimal _balance;

        /// <summary>
        /// Номер
        /// </summary>
        private readonly int _id;

        /// <summary>
        /// Тип
        /// </summary>
        private BankAccountType _type;

        public BankAccount()
        {
            _id = IdGenerator.GetNextId();
        }

        /// <summary>
        /// Положить на счет
        /// </summary>
        /// <param name="amount">сумма</param>
        public void Deposit(decimal amount)
        {
            _balance += amount;
        }

        /// <summary>
        /// Снять со счета
        /// </summary>
        /// <param name="amount">сумма</param>
        /// <returns>false - если снять не возможно, true - если все ok</returns>
        public bool Withdraw(decimal amount)
        {
            if (amount > _balance) return false;
            _balance -= amount;
            return true;
        }

        /// <summary>
        /// Установка типа счета
        /// </summary>
        /// <param name="type">тип счета</param>
        public void SetAccountType(BankAccountType type)
        {
            _type = type;
        }

        /// <summary>
        /// Полуить тип счета
        /// </summary>
        /// <returns>тип счета</returns>
        public BankAccountType GetAccountType()
        {
            return _type;
        }

        /// <summary>
        /// Получить номер счета
        /// </summary>
        /// <returns>номер счета</returns>
        public int GetId()
        {
            return _id;
        }

        /// <summary>
        ///     Возвращает строку, представляющую текущий объект.
        /// </summary>
        /// <returns>
        ///     Строка, представляющая текущий объект.
        /// </returns>
        public override string ToString()
        {
            var accType = string.Empty;
            switch (_type)
            {
                case BankAccountType.Current:
                    accType = "Текущий";
                    break;
                case BankAccountType.Payment:
                    accType = "Расчетный";
                    break;
                case BankAccountType.Budget:
                    accType = "Бюджетный";
                    break;
                case BankAccountType.Сorrespondent:
                    accType = "Корреспондентский";
                    break;
            }
            return String.Format("Номер: {0}\t Тип счета: {1}\t Баланс: {2}", _id, accType, _balance);
        }
    }
}