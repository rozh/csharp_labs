﻿using System;
using System.Collections.Generic;

namespace z7
{
    class Program
    {
        static void Main(string[] args)
        {
            var accounts = new List<BankAccount>();
            var acc = new BankAccount();
            acc.SetAccountType(BankAccountType.Payment);
            acc.Deposit(123);
            accounts.Add(acc);

            acc = new BankAccount();
            acc.SetAccountType(BankAccountType.Current);
            acc.Deposit(321);
            accounts.Add(acc);

            acc = new BankAccount();
            acc.SetAccountType(BankAccountType.Budget);
            acc.Deposit((decimal) 32313123.3);
            accounts.Add(acc);

            InsertLine();
            foreach (var bankAccount in accounts)
            {
                Console.WriteLine(bankAccount.ToString());
                InsertLine();
            }

            var buildings = new List<Building>();
            var buiding = new Building();
            buiding.SeFloorCount(4);
            buiding.SetDoorCount(2);
            buiding.SetFlatCount(32);
            buiding.SetHeight(28);
            buildings.Add(buiding);

            buiding = new Building();
            buiding.SeFloorCount(30);
            buiding.SetDoorCount(1);
            buiding.SetFlatCount(60);
            buiding.SetHeight(80);
            buildings.Add(buiding);

            foreach (var b in buildings)
            {
                Console.WriteLine(b.ToString());
                InsertLine();
            }
            Console.ReadKey();
        }

        static void InsertLine()
        {
            for (int i = 0; i < Console.WindowWidth - 1; i++)
            {
                Console.Write("-");
            }
            Console.WriteLine();
        }
    }
}
