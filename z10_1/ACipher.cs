﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z10_1
{
    abstract class ACipher : ICipher
    {
        /// <summary>
        /// Шифрует строку
        /// </summary>
        /// <param name="input">входная строка</param>
        /// <returns>результат шифрования</returns>
        public string Encode(string input)
        {
            var output = new StringBuilder();
            for (int i = 0; i < input.Length; i++)
            {
                output.Append((char)(input[i] + 1));
            }
            return output.ToString();
        }

        /// <summary>
        /// Расшифровывает строку
        /// </summary>
        /// <param name="input">входная строка</param>
        /// <returns>результат дешифрования</returns>
        public string Decode(string input)
        {
            var output = new StringBuilder();
            for (int i = 0; i < input.Length; i++)
            {
                output.Append((char)(input[i] - 1));
            }
            return output.ToString();
        }
    }
}
