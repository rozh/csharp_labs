﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z10_1
{
    abstract class BCipher : ICipher
    {
        /// <summary>
        /// Шифрует строку
        /// </summary>
        /// <param name="input">входная строка</param>
        /// <returns>результат шифрования</returns>
        public string Encode(string input)
        {
            return Code(input);
        }

        /// <summary>
        /// Расшифровывает строку
        /// </summary>
        /// <param name="input">входная строка</param>
        /// <returns>результат дешифрования</returns>
        public string Decode(string input)
        {
            return Code(input);
        }

        string Code(string input)
        {
            var output = new StringBuilder();
            for (int i = 0; i < input.Length; i++)
            {
                char ch = input[i];
                int index = ch - GetStartOfAlphabelt(ch);
                int lastIndex = GetEndOfAlphabelt(ch);
                output.Append((char)(lastIndex - index));
            }
            return output.ToString();
        }

        protected bool IsEnglish(char ch)
        {
            if (ch >= 'a' && ch <= 'z') return true;
            return false;
        }

        protected int GetStartOfAlphabelt(char ch)
        {
            if (ch >= 'a' && ch <= 'z') return 'a';
            if (ch >= 'A' && ch <= 'Z') return 'A';
            if (ch >= 'а' && ch <= 'я') return 'а';
            if (ch >= 'А' && ch <= 'Я') return 'А';
            return 0;
        }

        protected int GetEndOfAlphabelt(char ch)
        {
            if (ch >= 'a' && ch <= 'z') return 'z';
            if (ch >= 'A' && ch <= 'Z') return 'Z';
            if (ch >= 'а' && ch <= 'я') return 'я';
            if (ch >= 'А' && ch <= 'Я') return 'Я';
            return 0;
        }
    }
}
