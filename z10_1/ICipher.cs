﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z10_1
{
    interface ICipher
    {
        /// <summary>
        /// Шифрует строку
        /// </summary>
        /// <param name="input">входная строка</param>
        /// <returns>результат шифрования</returns>
        string Encode(string input);

        /// <summary>
        /// Расшифровывает строку
        /// </summary>
        /// <param name="input">входная строка</param>
        /// <returns>результат дешифрования</returns>
        string Decode(string input);
    }
}
