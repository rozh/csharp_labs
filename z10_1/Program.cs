﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z10_1
{
    class Program
    {
        static void Main(string[] args)
        {
            CesarChiper cc = new CesarChiper();
            string outstr = cc.Encode("абвгд");
            Console.WriteLine(outstr);
            Console.WriteLine(cc.Decode(outstr));
            InvertChiper ic = new InvertChiper();
            outstr = ic.Encode("абвгдя");
            Console.WriteLine(outstr);
            Console.WriteLine(ic.Decode(outstr));
            Console.ReadKey();
        }
    }
}
