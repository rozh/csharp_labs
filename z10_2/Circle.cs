﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z10_2
{
    class Circle : Point, ISquareCompute
    {
        public int Raduis { get; protected set; }

        public Circle(int x, int y, int r) : base(x, y)
        {
            Raduis = r;
        }

        public float GetSquare()
        {
            return (float)(Math.PI*Raduis*Raduis);
        }

        public override void Draw()
        {
            Console.WriteLine("Radius = {0}", Raduis);
            base.Draw();
        }
    }
}
