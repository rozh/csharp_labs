﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z10_2
{
    abstract class Figure
    {
        public Color Color { get; protected set; }
        public bool IsVisible { get; protected set; }

        public virtual void MoveHorizontally(int coord)
        { }
        public virtual void MoveVerticaly(int coord)
        { }

        public void SetColor(Color color)
        {
            Color = color;
        }

        public void ToggleVisibity()
        {
            IsVisible = !IsVisible;
        }
            
        public virtual void Draw()
        {
            Console.WriteLine(string.Format("Color: {0}\tIsVisible: {1}", Color, IsVisible));
            Console.WriteLine();
        }
    }
}
