﻿namespace z10_2
{
    interface ISquareCompute
    {
        float GetSquare();
    }
}