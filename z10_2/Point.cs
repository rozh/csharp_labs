﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z10_2
{
    class Point : Figure
    {
        private int _x = 0;
        private int _y = 0;

        public int X { get { return _x; } }
        public int Y { get { return _y; } }

        public Point(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public override void MoveHorizontally(int coord)
        {
            _x += coord;
            base.MoveHorizontally(coord);
        }

        public override void MoveVerticaly(int coord)
        {
            _y += coord;
            base.MoveVerticaly(coord);
        }

        public override void Draw()
        {
            Console.WriteLine(string.Format("X: {0}\tY: {1}", _x, _y));
            base.Draw();
        }
    }
}
