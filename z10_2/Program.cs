﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z10_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle circle = new Circle(10,10,5);
            circle.Draw();

            circle.SetColor(Color.Crimson);
            circle.MoveHorizontally(10);
            circle.MoveVerticaly(10);
            circle.Draw();

            Console.WriteLine("Square = {0}", circle.GetSquare());
            Console.WriteLine();

            Rectangle rectangle = new Rectangle(5,5,10,10);
            rectangle.Draw();

            rectangle.SetColor(Color.Black);
            rectangle.ToggleVisibity();
            rectangle.MoveVerticaly(-10);
            rectangle.Draw();

            Console.WriteLine("Square = {0}", rectangle.GetSquare());
            Console.ReadKey();
        }
    }
}
