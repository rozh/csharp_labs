﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z10_2
{
    class Rectangle : Point, ISquareCompute
    {
        public int Height { get; protected set; }
        public int Width { get; protected set; }

        public Rectangle(int x, int y, int height, int width) : base(x, y)
        {
            Height = height;
            Width = width;
        }

        public float GetSquare()
        {
            return (float)(Height*Width);
        }

        public override void Draw()
        {
            Console.WriteLine("Height = {0}\t Width = {1}", Height, Width);
            base.Draw();
        }
    }
}
