﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z11_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var bank = new Bank();
            bank.CreateAccount();
            var acc = bank.CreateAccount(BankAccountType.Current, 123);
            bank.CloseAccount(acc.GetId());
        }
    }
}
