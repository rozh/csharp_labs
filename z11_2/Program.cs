﻿using z11_2_assembly;

namespace z11_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var bank = new Bank();
            bank.CreateAccount();
            var acc = bank.CreateAccount(BankAccountType.Current, 123);
            bank.CloseAccount(acc.GetId());
        }
    }
}
