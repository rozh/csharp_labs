﻿using System.Collections;

namespace z11_2_assembly
{
    public class Bank
    {
        private readonly Hashtable _accounts = new Hashtable();

        public BankAccount CreateAccount()
        {
            var opened = new BankAccount();
            _accounts[opened.GetId()] = opened;
            return opened;
        }

        public BankAccount CreateAccount(BankAccountType bankAccount)
        {
            var opened = new BankAccount(bankAccount);
            return BankAccount(opened);
        }

        private BankAccount BankAccount(BankAccount opened)
        {
            _accounts[opened.GetId()] = opened;
            return opened;
        }

        public BankAccount CreateAccount(BankAccountType bankAccount, decimal amount)
        {
            var opened = new BankAccount(bankAccount, amount);
            return BankAccount(opened);
        }

        public BankAccount CreateAccount(decimal amount)
        {
            var opened = new BankAccount(amount);
            return BankAccount(opened);
        }

        //public void CloseAccaunt(BankAccount account)
        //{
        //    var accId = account.GetId();
        //    CloseAccaunt(accId);
        //}

        public void CloseAccount(int accountId)
        {
            if (_accounts.ContainsKey(accountId)) _accounts.Remove(accountId);
        }
    }
}