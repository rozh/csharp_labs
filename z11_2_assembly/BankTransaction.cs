﻿using System;

namespace z11_2_assembly
{
    class BankTransaction
    {
        public DateTime Date { get; protected set; }
        public BankTransactionAction Action { get; protected set; }
        public decimal Amount { get; protected set; }

        public BankTransaction(BankTransactionAction action, decimal amount)
        {
            Action = action;
            Amount = amount;
            Date = DateTime.Now;
        }

        public string ToFileLine()
        {
            return String.Format("[{0}]\t{1}\t{2}", Date.ToString("F"), Action, Amount);
        }
    }

    enum BankTransactionAction
    {
        Deposit,
        Withdraw
    }
}
