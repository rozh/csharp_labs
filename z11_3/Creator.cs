﻿using System;
using System.Collections;

namespace z11_3
{
    internal static class Creator
    {
        private static readonly Hashtable Buildings;

        static Creator()
        {
            Buildings = new Hashtable();
        }

        public static Building CreateBuild()
        {
            var build = new Building();
            return AddBiuld(build);
        }

        public static Building CreateBuild(int floorCount, int doorCount, int flatCount, float height)
        {
            var build = new Building();
            build.SetDoorCount(doorCount);
            build.SetFloorCount(floorCount);
            build.SetHeight(height);
            build.SetFlatCount(flatCount);

            return AddBiuld(build);
        }

        private static Building AddBiuld(Building build)
        {
            Buildings[build.GetId()] = build;
            return build;
        }

        public static void DestroyBuild(int buildId)
        {
            if (Buildings.ContainsKey(buildId)) Buildings.Remove(buildId);
        }

        public static void PrintBuildings()
        {
            foreach (var b in Buildings.Values)
            {
                Console.WriteLine(b.ToString());

                for (int i = 0; i < Console.WindowWidth - 1; i++)
                {
                    Console.Write("-");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}