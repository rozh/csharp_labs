﻿using System;
using System.Collections.Generic;

namespace z11_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Creator.CreateBuild();
            Creator.CreateBuild(4, 2, 32, 28);
            Creator.CreateBuild(4, 2, 32, 28);
            Creator.CreateBuild(30, 1, 60, 80);
            Creator.CreateBuild(4, 3, 32, 56);

            Creator.PrintBuildings();

            Console.ReadKey();
        }
    }
}
