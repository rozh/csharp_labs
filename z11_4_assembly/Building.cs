﻿using System;

namespace z11_3
{
    /// <summary>
    /// Здание
    /// </summary>
    public class Building
    {
        /// <summary>
        /// Количество подъездов
        /// </summary>
        private int _doorCount;

        /// <summary>
        /// КОличество квартир
        /// </summary>
        private int _flatCount;

        /// <summary>
        /// КОличество этажей
        /// </summary>
        private int _floorCount;

        /// <summary>
        /// Высота здания
        /// </summary>
        private float _height;

        /// <summary>
        /// Идентификатор
        /// </summary>
        private readonly int _id;

        internal Building()
        {
            _id = IdGenerator.GetNextId();
        }

        public int GetId()
        {
            return _id;
        }

        public int GetDoorCount()
        {
            return _doorCount;
        }

        public int GetFloorCount()
        {
            return _floorCount;
        }

        public int GetFlatCount()
        {
            return _flatCount;
        }

        public float GetHeight()
        {
            return _height;
        }

        public void SetDoorCount(int count)
        {
            _doorCount = count;
        }

        public void SetFloorCount(int count)
        {
            _floorCount = count;
        }

        public void SetFlatCount(int count)
        {
            _flatCount = count;
        }

        public void SetHeight(float height)
        {
            _height = height;
        }

        public float GetFloorHeight()
        {
            return _height/_floorCount;
        }

        public float GetFlatsInDoor()
        {
            return (float) _flatCount/_doorCount;
        }

        public float GetFlatsInFloor()
        {
            return (float) _flatCount/_floorCount;
        }

        /// <summary>
        ///     Возвращает строку, представляющую текущий объект.
        /// </summary>
        /// <returns>
        ///     Строка, представляющая текущий объект.
        /// </returns>
        public override string ToString()
        {
            return
                String.Format("Номер: {0}\t Высота: {1}\t Этажность: {2}\t Квартир {3}\t Подъездов: {4}\n", _id, _height,
                    _floorCount, _flatCount, _doorCount) +
                String.Format("Высота этажа: {0}\t Квартир в подъезде: {1}\t Квартир на этаже: {2}", GetFloorHeight(),
                    GetFlatsInDoor(), GetFlatsInFloor());
        }
    }
}