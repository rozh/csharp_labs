﻿namespace z11_3
{
    /// <summary>
    /// Генератор уникальных номеров
    /// </summary>
    static class IdGenerator
    {
        /// <summary>
        /// Последний ID
        /// </summary>
        private static int _currentId;

        /// <summary>
        /// Получить новый ID
        /// </summary>
        /// <returns>новый ID</returns>
        public static int GetNextId()
        {
            return ++_currentId;
        }
    }
}
