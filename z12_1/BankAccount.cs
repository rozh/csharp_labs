﻿using System;
using System.Collections.Generic;
using System.IO;

namespace z12_1
{
    /// <summary>
    /// Типы счетов
    /// </summary>
    enum BankAccountType
    {
        /// <summary>
        /// Текущий
        /// </summary>
        Current,

        /// <summary>
        /// Расчетный
        /// </summary>
        Payment,

        /// <summary>
        /// Бюджетный
        /// </summary>
        Budget,

        /// <summary>
        /// Корреспондентский
        /// </summary>
        Сorrespondent
    }

    /// <summary>
    /// Счет
    /// </summary>
    class BankAccount : IDisposable
    {
        /// <summary>
        /// Баланс
        /// </summary>
        private decimal _balance;

        /// <summary>
        /// Номер
        /// </summary>
        private readonly int _id;

        /// <summary>
        /// Тип
        /// </summary>
        private BankAccountType _type;

        private Queue<BankTransaction> _transactions; 

        /// <summary>
        /// Счет
        /// </summary>
        internal BankAccount()
        {
            _id = IdGenerator.GetNextId();
            _transactions = new Queue<BankTransaction>();
        }

        /// <summary>
        /// Счет
        /// </summary>
        /// <param name="type">Тип счета</param>
        /// <param name="amount">баланс</param>
        public BankAccount(BankAccountType type, decimal amount) : this()
        {
            _type = type;
            _balance = amount;
        }

        /// <summary>
        /// Счет
        /// </summary>
        /// <param name="type">Тип счета</param>
        public BankAccount(BankAccountType type) : this(type, Decimal.Zero)
        { }

        /// <summary>
        /// Счет
        /// </summary>
        /// <param name="amount">баланс</param>
        public BankAccount(decimal amount) : this(BankAccountType.Payment, amount)
        { }

        /// <summary>
        /// Положить на счет
        /// </summary>
        /// <param name="amount">сумма</param>
        public void Deposit(decimal amount)
        {
            _balance += amount;
            _transactions.Enqueue(new BankTransaction(BankTransactionAction.Deposit, amount));
        }

        /// <summary>
        /// Снять со счета
        /// </summary>
        /// <param name="amount">сумма</param>
        /// <returns>false - если снять не возможно, true - если все ok</returns>
        public bool Withdraw(decimal amount)
        {
            if (amount > _balance) return false;
            _balance -= amount;
            _transactions.Enqueue(new BankTransaction(BankTransactionAction.Withdraw, amount));
            return true;
        }
        
        /// <summary>
        /// Полуить тип счета
        /// </summary>
        /// <returns>тип счета</returns>
        public BankAccountType GetAccountType()
        {
            return _type;
        }

        /// <summary>
        /// Получить номер счета
        /// </summary>
        /// <returns>номер счета</returns>
        public int GetId()
        {
            return _id;
        }

        /// <summary>
        /// Перевод средств
        /// </summary>
        /// <param name="from">откуда</param>
        /// <param name="amount">сколько</param>
        /// <returns>true - если удачно</returns>
        public bool Transfer(BankAccount from, decimal amount)
        {
            var result = from.Withdraw(amount);
            if (result)
            {
                Deposit(amount);
            }
            return result;
        }

        /// <summary>
        ///     Возвращает строку, представляющую текущий объект.
        /// </summary>
        /// <returns>
        ///     Строка, представляющая текущий объект.
        /// </returns>
        public override string ToString()
        {
            var accType = string.Empty;
            switch (_type)
            {
                case BankAccountType.Current:
                    accType = "Текущий";
                    break;
                case BankAccountType.Payment:
                    accType = "Расчетный";
                    break;
                case BankAccountType.Budget:
                    accType = "Бюджетный";
                    break;
                case BankAccountType.Сorrespondent:
                    accType = "Корреспондентский";
                    break;
            }
            return String.Format("Номер: {0}\t Тип счета: {1}\t Баланс: {2}", _id, accType, _balance);
        }

        /// <summary>
        /// Выполняет определяемые приложением задачи, связанные с высвобождением или сбросом неуправляемых ресурсов.
        /// </summary>
        public void Dispose()
        {
            var sw = File.AppendText(_id + ".txt");
            while (_transactions.Count>0)
            {
                var transaction = _transactions.Dequeue();
                sw.WriteLine(transaction.ToFileLine());
            }
            sw.Close();
            GC.SuppressFinalize(_transactions);
        }

        /// <summary>
        /// Определяет, равен ли заданный объект текущему объекту.
        /// </summary>
        /// <returns>
        /// true, если указанный объект равен текущему объекту; в противном случае — false.
        /// </returns>
        /// <param name="obj">Объект, который требуется сравнить с текущим объектом. </param>
        public override bool Equals(object obj)
        {
            if (obj is BankAccount)
                return Equals((BankAccount) obj);

            return base.Equals(obj);
        }

        protected bool Equals(BankAccount other)
        {
            return _balance == other._balance && _id == other._id && _type == other._type && Equals(_transactions, other._transactions);
        }

        /// <summary>
        /// Служит хэш-функцией по умолчанию. 
        /// </summary>
        /// <returns>
        /// Хэш-код для текущего объекта.
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = _balance.GetHashCode();
                hashCode = (hashCode*397) ^ _id;
                hashCode = (hashCode*397) ^ (int) _type;
                hashCode = (hashCode*397) ^ (_transactions != null ? _transactions.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(BankAccount bankAccount1, BankAccount bankAccount2)
        {
            return Equals(bankAccount1, bankAccount2);
        }

        public static bool operator !=(BankAccount bankAccount1, BankAccount bankAccount2)
        {
            return !Equals(bankAccount1, bankAccount2);
        }
    }
}