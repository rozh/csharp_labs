﻿using System;

namespace z12_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var ba1 = new BankAccount(BankAccountType.Current, 123);
            var ba2 = new BankAccount(BankAccountType.Current, 123);
            var ba3 = new BankAccount(BankAccountType.Payment, 32);

            Console.WriteLine(ba1.ToString());
            Console.WriteLine(ba2.ToString());
            Console.WriteLine(ba3.ToString());

            if (ba1==ba1)
                Console.WriteLine("ba1 == ba1");
            if (ba1 != ba1)
                Console.WriteLine("ba1 != ba1");
            Console.WriteLine(ba2 != ba1 ? "ba2 != ba1" : "ba2 == ba1");
            Console.WriteLine(ba2 == ba1 ? "ba2 == ba1" : "ba2 != ba1");
            Console.WriteLine(ba3 == ba1 ? "ba3 == ba1" : "ba3 != ba1");
        }
    }
}
