﻿namespace z12_2
{
    internal class Racio
    {
        public int Znamenatel { get; set; }
        public int Chislitel { get; set; }

        public static Racio operator +(Racio r1, Racio r2)
        {
            if (r1.Znamenatel == r2.Znamenatel)
            {
                return new Racio
                {
                    Chislitel = r1.Chislitel + r2.Chislitel,
                    Znamenatel = r1.Znamenatel
                };
            }

            var nok = NOK(r1.Znamenatel, r2.Znamenatel);
            return new Racio
            {
                Znamenatel = nok,
                Chislitel = r1.Chislitel*nok/r1.Znamenatel + r2.Chislitel*nok/r2.Znamenatel
            };
        }

        public static Racio operator -(Racio r1, Racio r2)
        {
            if (r1.Znamenatel == r2.Znamenatel)
            {
                return new Racio
                {
                    Chislitel = r1.Chislitel - r2.Chislitel,
                    Znamenatel = r1.Znamenatel
                };
            }

            var nok = NOK(r1.Znamenatel, r2.Znamenatel);
            return new Racio
            {
                Znamenatel = nok,
                Chislitel = r1.Chislitel*nok/r1.Znamenatel - r2.Chislitel*nok/r2.Znamenatel
            };
        }

        public static Racio operator ++(Racio r1)
        {
            return new Racio
            {
                Chislitel = r1.Chislitel + r1.Znamenatel,
                Znamenatel = r1.Znamenatel
            };
        }

        public static Racio operator --(Racio r1)
        {
            return new Racio
            {
                Chislitel = r1.Chislitel - r1.Znamenatel,
                Znamenatel = r1.Znamenatel
            };
        }

        private static int NOK(int a, int b)
        {
            if (a >= b)
            {
                for (var i = a; i >= a; i++)
                {
                    if (i%a == 0)
                    {
                        if (i%b == 0)
                        {
                            return i;
                        }
                    }
                }
            }
            else
            {
                for (var i = b; i >= b; i++)
                {
                    if (i%b == 0)
                    {
                        if (i%a == 0)
                        {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }


        public static bool operator ==(Racio r1, Racio r2)
        {
            return Equals(r1, r2);
        }

        public static bool operator !=(Racio r1, Racio r2)
        {
            return !(r1 == r2);
        }

        public static bool operator >=(Racio r1, Racio r2)
        {
            if (Equals(r1, r2)) return true;
            return r1 > r2;
        }

        public static bool operator <=(Racio r1, Racio r2)
        {
            if (Equals(r1, r2)) return true;
            return r1 < r2;
        }

        public static bool operator >(Racio r1, Racio r2)
        {
            var nok = NOK(r1.Znamenatel, r2.Znamenatel);
            var ch1 = r1.Chislitel*nok/r1.Znamenatel;
            var ch2 = r2.Chislitel*nok/r2.Znamenatel;

            return ch1 > ch2;
        }

        public static bool operator <(Racio r1, Racio r2)
        {
            var nok = NOK(r1.Znamenatel, r2.Znamenatel);
            var ch1 = r1.Chislitel*nok/r1.Znamenatel;
            var ch2 = r2.Chislitel*nok/r2.Znamenatel;

            return ch1 < ch2;
        }

        public static Racio operator *(Racio r1, Racio r2)
        {
            return new Racio()
            {
                Chislitel = r1.Chislitel * r2.Chislitel,
                Znamenatel = r1.Znamenatel * r2.Znamenatel
            };
        }

        public static Racio operator /(Racio r1, Racio r2)
        {
            return new Racio()
            {
                Chislitel = r1.Chislitel * r2.Znamenatel,
                Znamenatel = r1.Znamenatel * r2.Chislitel
            };
        }

        //TODO Как делить дробь без остатка? 0_o
        public static Racio operator %(Racio r1, Racio r2)
        {
            return new Racio()
            {
                Chislitel = r1.Chislitel * r2.Znamenatel,
                Znamenatel = r1.Znamenatel * r2.Chislitel
            };
        }

        /// <summary>
        ///     Определяет, равен ли заданный объект текущему объекту.
        /// </summary>
        /// <returns>
        ///     true, если указанный объект равен текущему объекту; в противном случае — false.
        /// </returns>
        /// <param name="obj">Объект, который требуется сравнить с текущим объектом. </param>
        public override bool Equals(object obj)
        {
            if (obj is Racio)
                return Equals((Racio) obj);

            return base.Equals(obj);
        }

        protected bool Equals(Racio other)
        {
            return Znamenatel == other.Znamenatel && Chislitel == other.Chislitel;
        }

        /// <summary>
        ///     Служит хэш-функцией по умолчанию.
        /// </summary>
        /// <returns>
        ///     Хэш-код для текущего объекта.
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return (Znamenatel*397) ^ Chislitel;
            }
        }

        /// <summary>
        /// Возвращает строку, представляющую текущий объект.
        /// </summary>
        /// <returns>
        /// Строка, представляющая текущий объект.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{0}/{1}", Chislitel, Znamenatel);
        }
    }
}