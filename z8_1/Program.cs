﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z8_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var accounts = new List<BankAccount>();
            var acc = new BankAccount();
            acc.SetAccountType(BankAccountType.Payment);
            acc.Deposit(123);
            accounts.Add(acc);

            var acc2 = new BankAccount();
            acc2.SetAccountType(BankAccountType.Current);
            acc2.Deposit(321);
            accounts.Add(acc2);

            var acc3 = new BankAccount();
            acc3.SetAccountType(BankAccountType.Budget);
            acc3.Deposit((decimal)32313123.3);
            accounts.Add(acc3);

            InsertLine();
            foreach (var bankAccount in accounts)
            {
                Console.WriteLine(bankAccount.ToString());
                InsertLine();
            }
            Console.WriteLine();

            acc2.Transfer(acc, 100);

            InsertLine();
            foreach (var bankAccount in accounts)
            {
                Console.WriteLine(bankAccount.ToString());
                InsertLine();
            }
            Console.WriteLine();

            acc3.Transfer(acc2, 300);

            InsertLine();
            foreach (var bankAccount in accounts)
            {
                Console.WriteLine(bankAccount.ToString());
                InsertLine();
            }
            Console.ReadKey();
        }

        static void InsertLine()
        {
            for (int i = 0; i < Console.WindowWidth - 1; i++)
            {
                Console.Write("-");
            }
            Console.WriteLine();
        }
    }
}
