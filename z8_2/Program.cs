﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z8_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Inverse("12345"));
            Console.ReadKey();
        }

        static string Inverse(string input)
        {
            var sb = new StringBuilder(input.Length);
            for (int i = input.Length-1; i >= 0 ; i--)
            {
                sb.Append(input[i]);
            }
            return sb.ToString();
        }
    }
}
