﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z8_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Имя файла: ");
            var fileName = Console.ReadLine();
            if (!File.Exists(fileName))
            {
                Console.WriteLine("Нет такого файла!");
            }
            else
            {
                var str = File.ReadAllText(fileName);
                var name =  Path.GetFileNameWithoutExtension(fileName);
                File.WriteAllText(name + "_up.txt", str.ToUpperInvariant());
            }
        }
    }
}
