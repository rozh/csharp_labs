﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z8_4
{
    class Program
    {
        static void Main(string[] args)
        {
            var obj = new DateTime();
            Console.WriteLine(CheckIFormattable(obj));
            var obj2 = new object();
            Console.WriteLine(CheckIFormattable(obj2));
        }

        static bool CheckIFormattable(object obj)
        {
            //return (obj as IFormattable) != null;
            return obj is IFormattable;
        }
    }
}
