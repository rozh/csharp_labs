﻿using System.IO;

namespace z8_5
{
    class Program
    {
        static void Main(string[] args)
        {
            var str = File.ReadAllLines("file.txt");
            for (int i = 0; i < str.Length; i++)
            {
                SearchMail(ref str[i]);
            }
            File.WriteAllLines("file_email.txt", str);
        }

        static void SearchMail(ref string s)
        {
            var strs = s.Split(new char[] {'#'});
            s = strs.Length>1 ? strs[1].Trim() : string.Empty;
        }
    }
}
