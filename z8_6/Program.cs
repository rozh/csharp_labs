﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace z8_6
{
    class Program
    {
        static void Main(string[] args)
        {
            var song = new Song();
            song.SetAuthor("Кипелов");
            song.SetName("Я свободен!");
            var song2 = new Song();
            song2.SetAuthor("Кипелов");
            song2.SetName("Безумие");
            song.SetPrevSong(song2);
            var song3 = new Song();
            song3.SetAuthor("Digimortal");
            song3.SetName("Клон");
            song2.SetPrevSong(song3);
            var song4 = new Song();
            song4.SetAuthor("Digimortal");
            song4.SetName("3 килобайта");
            song3.SetPrevSong(song4);

            var curSong = song;

            while (curSong!=null)
            {
                Console.WriteLine(curSong.Title());
                curSong = curSong.GetPrevSong();
            }

            Console.WriteLine(song.Equals(song.GetPrevSong()));
            Console.ReadKey();
        }
    }
}
