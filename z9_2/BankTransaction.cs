﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z9_2
{
    class BankTransaction
    {
        public DateTime Date { get; protected set; }
        public BankTransactionAction Action { get; protected set; }
        public decimal Amount { get; protected set; }

        public BankTransaction(BankTransactionAction action, decimal amount)
        {
            Action = action;
            Amount = amount;
            Date = DateTime.Now;
        }
    }

    enum BankTransactionAction
    {
        Deposit,
        Withdraw
    }
}
