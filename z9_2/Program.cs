﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z9_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var accounts = new List<BankAccount>();
            var acc = new BankAccount(BankAccountType.Payment, 123);
            acc.Deposit(444);
            accounts.Add(acc);

            var acc2 = new BankAccount(BankAccountType.Current, 321);
            acc2.Withdraw(100);
            accounts.Add(acc2);

            var acc3 = new BankAccount(222);
            accounts.Add(acc3);

            InsertLine();
            foreach (var bankAccount in accounts)
            {
                Console.WriteLine(bankAccount.ToString());
                InsertLine();
            }
            Console.ReadKey();
        }

        static void InsertLine()
        {
            for (int i = 0; i < Console.WindowWidth - 1; i++)
            {
                Console.Write("-");
            }
            Console.WriteLine();
        }
    }
}
