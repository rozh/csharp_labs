﻿using System;

namespace z9_4
{
    class Program
    {
        static void Main(string[] args)
        {
            var song = new Song();
            song.SetAuthor("Кипелов");
            song.SetName("Я свободен!");
            var song2 = new Song("Безумие", "Кипелов");
            song2.SetPrevSong(song);
            var song3 = new Song("Клон", "Digimortal", song2);
            var song4 = new Song("3 килобайта", "Digimortal", song3);

            var curSong = song4;

            while (curSong!=null)
            {
                Console.WriteLine(curSong.Title());
                curSong = curSong.GetPrevSong();
            }

            Console.WriteLine(song4.Equals(song4.GetPrevSong()));
            Console.ReadKey();
        }
    }
}
