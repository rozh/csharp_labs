﻿using System;

namespace z9_4
{
    class Song
    {
        string _name; //название песни
        string _author; //автор песни
        Song _prev; //связь с предыдущей песней в списке

        public Song()
        {
            _name = "Unknown";
            _author = "Unknown";
            _prev = null;
        }

        public Song(string name, string author, Song prev)
        {
            _name = name;
            _author = author;
            _prev = prev;
        }

        public Song(string name, string author) : this(name,author,null)
        { }

        public void SetName(string name)
        {
            _name = name;
        }
        public void SetAuthor(string author)
        {
            _author = author;
        }
        public void SetPrevSong(Song prev)
        {
            _prev = prev;
        }
        public string GetName()
        {
            return _name;
        }
        public string GetAuthor()
        {
            return _author;
        }
        public Song GetPrevSong()
        {
           return _prev;
        }

        public string Title()
        {
        /*возвращ название+исполнитель*/
            return GetName() + " " + GetAuthor();
        }
        //метод, который сравнивает между собой два объекта-песни:
        public override bool Equals(object d)
        {
            var other = (Song) d;
            return 
                string.Equals(_name, other._name, StringComparison.InvariantCultureIgnoreCase) && 
                string.Equals(_author, other._author, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
